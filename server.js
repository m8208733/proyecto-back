//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Credentials","*");
  res.header("Access-Control-Allow-Methods","*");
  res.header("Access-Control-Allow-Headers","Origin,X-Requested-With,Content-Type,Accept");

  next();
});



var requestjson = require('request-json');
var urlmovimientosMLab = "https://api.mlab.com/api/1/databases/bdbanca4m820873/collections/movimientos?apiKey=QgdaC09U9POTko2fpyLFhx5LYYMN4I7O";
//var urlmovimientosMLab = "https://mlab.com/databases/bdbanca4m820873/collections/movimientos?apiKey=QgdaC09U9POTko2fpyLFhx5LYYMN4I7O";

var clienteMLab = requestjson.createClient(urlmovimientosMLab);



var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/",function(req, res){
  res.sendFile(path.join(__dirname,'index.html'));
});

app.get("/clientes/:idcliente",function(req, res){
  res.send('Aqui tiene el cliente numero : ' + req.params.idcliente);
});

app.post("/",function(req,res){
    res.send('Hemos recibido su peticiòn cambiada');
});
app.delete("/",function(req,res){
    res.send('Hemos recibido su borrado');
});
app.put("/",function(req,res){
    res.send('Hemos recibido su actualizac');
});
//insertar o actualizar compras posicion cuenado hay compras
app.put("/movimientos/compras/:nombre",function(req, res){
  var nombre = req.params.nombre;
  clienteMLab.put(urlmovimientosMLab + "&q={nombre:'"+nombre+"'}",req.body,
  function (err, resM, body){
    if(err){
      console.log(body);
    } else {
      res.send(body);
      console.log(body);
      }
  });
});
//actualizar o eliminar posicion cuando hay ventas
app.put("/movimientos/ventas/:nombre",function(req, res){
  var nombre = req.params.nombre;
  clienteMLab.put(urlmovimientosMLab + "&q={nombre:'"+nombre+"'}",req.body,
  function (err, resM, body){
    if(err){
      console.log(body);
    } else {
      res.send(body);
      console.log(body);
      }
  });
});
//actualizar efectivo, insertando mas
app.put("/movimientos/recefec/:nombre",function(req, res){
  var nombre = req.params.nombre;
  clienteMLab.put(urlmovimientosMLab + "&q={nombre:'"+nombre+"'}",req.body,
  function (err, resM, body){
    if(err){
      console.log(body);
    } else {
      res.send(body);
      console.log(body);
      }
  });
});
//consultar la clave del cliente
app.get("/movimientos/clave/:nombre",function(req, res){
  var nombre = req.params.nombre;
  clienteMLab.get(urlmovimientosMLab + "&f={clave:1}&q={nombre:'"+nombre+"'}",
  function (err, resM, body){
    if(err){
      console.log(body);
    } else {
      res.send(body);
      }
  });
});
//consultar el efectivo del cliente
app.get("/movimientos/efectivo/:nombre",function(req, res){
  var nombre = req.params.nombre;
  clienteMLab.get(urlmovimientosMLab + "&f={efectivo:1}&q={nombre:'"+nombre+"'}",
  function (err, resM, body){
    if(err){
      console.log(body);
    } else {
      res.send(body);
      }
  });
});
//consultar el precio de venta registrado de la posicion del cliente
app.get("/movimientos/preciov/:nombre",function(req, res){
  var nombre = req.params.nombre;
  clienteMLab.get(urlmovimientosMLab + "&f={listado.preciov:1}&q={nombre:'"+nombre+"'}",
  function (err, resM, body){
    if(err){
      console.log(body);
    } else {
      res.send(body);
      }
  });
});
//consultar el precio de compra registrado de la posicion del cliente
app.get("/movimientos/precioc/:nombre",function(req, res){
  var nombre = req.params.nombre;
  clienteMLab.get(urlmovimientosMLab + "&f={listado.precioc:1}&q={nombre:'"+nombre+"'}",
  function (err, resM, body){
    if(err){
      console.log(body);
    } else {
      res.send(body);
      }
  });
});
//consultar la serie registrada de la posicion del cliente
app.get("/movimientos/serie/:nombre",function(req, res){
  var nombre = req.params.nombre;
  clienteMLab.get(urlmovimientosMLab + "&f={listado.serie:1}&q={nombre:'"+nombre+"'}",
  function (err, resM, body){
    if(err){
      console.log(body);
    } else {
      res.send(body);
      }
  });
});
//consultar la emisora registrada de la posicion del cliente
app.get("/movimientos/emisora/:nombre",function(req, res){
  var nombre = req.params.nombre;
  clienteMLab.get(urlmovimientosMLab + "&f={listado.emisora:1}&q={nombre:'"+nombre+"'}",
  function (err, resM, body){
    if(err){
      console.log(body);
    } else {
      res.send(body);
      }
  });
});
//consultar los titulos registrados de la posicion del cliente
app.get("/movimientos/titulos/:nombre",function(req, res){
  var nombre = req.params.nombre;
  clienteMLab.get(urlmovimientosMLab + "&f={listado.titulos:1}&q={nombre:'"+nombre+"'}",
  function (err, resM, body){
    if(err){
      console.log(body);
    } else {
      res.send(body);
      }
  });
});
//consultar si la combinacion usuario-contraseña existe en nuestra base de datos (login)
app.get("/movimientos/:nombre/:clave",function(req, res){
  var nombre = req.params.nombre;
  var clave = req.params.clave;
  clienteMLab.get(urlmovimientosMLab + "&q={nombre:'"+nombre+"', clave:'"+clave+"'}",
  function (err, resM, body){
    if(err){
      console.log(body);
    } else {
      res.send(body);
      }
  });
});
//consultar si el nombre existe en la base de datos (altauser)
app.get("/movimientos/:nombre",function(req, res){
  var nombre = req.params.nombre;
  clienteMLab.get(urlmovimientosMLab + "&q={nombre:'"+nombre+"'}",
  function (err, resM, body){
    if(err){
      console.log(body);
    } else {
      res.send(body);
      }
  });
});

app.get("/movimientos",function(req, res){
  clienteMLab.get('',function (err, resM, body){
    if(err){
      console.log(body);
    } else {
      res.send(body);
      }
  });
});

app.post("/movimientos",function(req,res){
  clienteMLab.post('', req.body, function (err, resM, body){
    res.send(body);
  });
});
